import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,} from 'rxjs';
import { menu } from '../side-menu/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http : HttpClient) { }

  //headers
  opciones = {
    headers: new HttpHeaders({
      'Content-Type':  'application/x-www-form-urlencoded',
    })
  };

  public fetchData(){
    return this.http.get<menu>('http://localhost:7431/menu', this.opciones);
    /*  TODO: Obtener información get de localhost:7431/home  */
  }


}