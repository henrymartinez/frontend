import { Component, OnInit } from '@angular/core';
//import { HttpClient } from '@angular/common/http'; //Pasamos esta linea al servicio
import { menu } from './side-menu/menu.model';
import { MenuService} from './servicios/menu.service'; // importamos el servcio

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'proyecta';
  sidemenu : menu; //Tipo menú

  constructor(private servicioMenu: MenuService) {}

  ngOnInit(){
    this.fetchData();
  }

  private fetchData() {
    new Promise(resolve => { this.servicioMenu.fetchData().subscribe(response => {
      this.sidemenu = response; //Se asigna la respuesta a lo que será el menú
    }); });
  }

 
}
